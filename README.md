# Pimeo : Gulp Starter
Date: 8 mars 2015

(projet inspiré du projet Gulp famous.us avec mise à jour et ajout nouvelles fonctionnalités)

## Fonctionnalités

- compilateur CoffeeScript
- prise en charge de Browserify avec l'ensemble des fichiers .coffee (utilisation de Vinyl)
- compilateur Sass Ruby (et non Node) pour plus de stabilité avec prise en charge Compass
- compilation Jade
- i18n avec compilation possible dans un fichier HTML différent par locale à partir d'un fichier .jade
- gestionnaire de réécriture d'url avec le module connect rewrite (router non implémenté de base dans le projet)
- compression / optimisation des images
- serveur connect sur serveur localhost et port 3000 avec prise en charge de Livereload

## Installation

Note :
- La GEM Sass doit être installée pour utiliser le starter
` gem install sass `

- La GEM Compass  doit être installée si vous voulez utiliser le framework SASS (par défaut activé)
` gem install compass `

```
git clone https://pimeo@bitbucket.org/pimeo/starter_gulp.git
npm install

# Lancement des tâches et démarrage du serveur comprenant Livereload
gulp watch 

# Lancement des tâches sans démarrage du serveur
gulp
```

## A venir

- processus de compilation GZIP des fichiers CSS / HTML / JS
- intégration du plugin gulp-notify pour la gestion des erreurs
- prévenir l'arrêt du serveur sur des erreurs CSS / HTML / JS


## Réflexion

- Remplacement de sass par stylus ?
