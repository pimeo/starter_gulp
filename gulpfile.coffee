'use strict'

gulp       = require 'gulp'
gp         = (require 'gulp-load-plugins') lazy: false
path       = require 'path'
browserify = require 'browserify'
source     = require 'vinyl-source-stream'
runSequence= require 'run-sequence'
modRewrite = require 'connect-modrewrite'

# Configuration paths
src = 
  styles   : 'src/styles/'
  images   : 'src/images/'
  scripts  : 'src/scripts/'
  templates: 'src/templates/'
  locales  : 'src/locales/'

# Configuration langs
langs =
  fr: 'fr'
  en: 'en'

# ----
# TASKS
# ----

# Connect
gulp.task 'connect', ['default'], ->
  gp.connect.server
    root: 'app'
    port: 3000
    livereload: true
    # url middleware
    middleware: ->
      [
        modRewrite [
          '^[^\\.]*$ /index.html [L]'
        ]
      ]

# JADE
gulp.task 'jade', ->
  gulp.src "#{src.templates}index.jade"
    .pipe gp.plumber()
    .pipe gp.jade 
      pretty: true
      basedir: "#{src.templates}"
      data: langs
      # locals: pageTitle: 'Famo.us built with Gulp'
    .pipe gulp.dest 'app'

# I18N
gulp.task 'i18n', ->
  gulp.src 'app/index.html'
  .pipe gp.htmlI18n
    langDir: "./#{src.locales}"
    trace: true
  .pipe gulp.dest './app/'

# DEFAULT HTML FILE
gulp.task 'rename-fr', ->
  gulp.src './app/index-fr.html'
  .pipe gp.rename 'index.html'
  .pipe gulp.dest './app/'

# HTML
gulp.task 'html', ->
  runSequence 'jade', 'i18n', 'rename-fr'

# BUILD JS AND COFFEE
gulp.task 'build-js', ->
  # vendors
  gulp.src "#{src.scripts}**/*.js"
  .pipe gp.concat "_vendor.js"
  .pipe gulp.dest "./tmp/"

  # sources
  browserify
    entries: ["./#{src.scripts}main.coffee"]
    extensions: ['.coffee', '.js']
  .transform 'coffeeify'
  .transform 'deamdify'
  .transform 'uglifyify'
  .bundle()
  # Pass desired file name to browserify with vinyl
  .pipe source 'main.js'
  # Start piping stream to tasks!
  .pipe gulp.dest 'tmp/'

# MIN JS
gulp.task 'min-js', ->
  gulp.src './tmp/*.js'
  .pipe gp.concat 'main.js'
  .pipe gulp.dest './app/js'

  return gulp.src "./tmp/*.js"
  .pipe gp.clean force: true

# JS
gulp.task 'js', ->
  runSequence 'build-js', 'min-js'

# CSS
gulp.task 'css', ['img'], ->
  # NEW RUBY SASS WITH 1.0.0-alpha
  # https://github.com/sindresorhus/gulp-ruby-sass/issues/164
  gp.rubySass src.styles,
    compass: true
    style: 'compressed'
    # loadPath: ['bower_components', '.']
  # .pipe gp.plumber()
  .on 'error', (err) ->
    console.log 'SASS ERROR', err
  .pipe gp.cssmin keepSpecialComments: 0
  .pipe gp.autoprefixer 'last 1 version'
  .pipe gp.rename suffix: '.min'
  .pipe gulp.dest 'app/css'

# Images
gulp.task 'img', ->
  gulp.src ["#{src.images}*.jpg", "#{src.images}app/img/*.png"]
    .pipe gp.cache gp.imagemin
      optimizationLevel: 3
      progressive: true
      interlaced: true
    .pipe gulp.dest 'app/img'

# Fonts
gulp.task 'fonts', ->
  gulp.src "#{src.fonts}fonts/*"
  .pipe gulp.dest 'app/css/fonts'

# Clean
gulp.task 'clean', ->
  gulp.src ['app', 'tmp'], read: false
    .pipe gp.clean force: true

# Watch
gulp.task 'watch', ['connect'], ->
  gulp.watch 'src/**/*', {read: false},
  (event) ->
    ext = path.extname event.path
    taskname = null
    reloadasset = null
    switch ext
      when '.jade'
        taskname = 'html'
        reloadasset = 'app/index.html'
      when '.scss'
        taskname = 'css'
        reloadasset = 'app/css/main.css'
      when '.coffee', '.js'
        taskname = 'js'
        reloadasset = 'app/js/main.js'
      else
        taskname = 'img'
        reloadasset = "app/img/#{path.basename(event.path)}"
    
    gulp.task 'reload', [taskname], ->
      gulp.src reloadasset
      .pipe gp.connect.reload()

    gulp.start 'reload' 

# ----
# COMMAND TASKS
# ----

# Build
gulp.task 'build', ->
  runSequence 'html', 'js', 'css', 'fonts'

# Default task
gulp.task 'default', ['clean'], -> gulp.start 'build'